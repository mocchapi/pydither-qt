
################ PyDither QT #################
# graphical IM frontend to preview dithering #
##############################################

Lets you preview different levels of dithering
with different dither options before saving

Author: Mocha (Anne)
Project source: https://gitlab.com/mocchapi/pydither-qt
License: Custom Anticapitalis License (based on v 1.4): see license.txt

requirements:
 - ImageMagick >= v7
 - pyqt5 >= 5.15.1


yes, extremely original name, i know