#!/usr/bin/python3
import os
from threading import Thread
import sys

from PyQt5.QtGui import QIcon, QPixmap
from PyQt5.QtWidgets import *
from PyQt5.QtCore import Qt


class LFileButton(QPushButton):
	def __init__(self,callfunc,text=''):
		super().__init__(text)
		self.callfunc = callfunc
		self.filemode=QFileDialog.AnyFile
		self.filter = 'Any file (*)'
		self.clicked.connect(self.onclick)

	def setFileMode(self,filemode):
		self.filemode = filemode

	def setNameFilter(self,ffilter):
		self.filter = ffilter

	def onclick(self):
		dlg = QFileDialog()
		dlg.setFileMode(self.filemode)
		dlg.setNameFilter(self.filter)
		files = []
		if dlg.exec_():
			files = dlg.selectedFiles()
		if files:
			self.callfunc(files)


class LImage(QLabel):
	def __init__(self,imgpath):
		super().__init__()
		self.imgpath = imgpath
		self.size = 100
		self.OGpixmap = QPixmap(self.imgpath)
		self.pixmap = self.OGpixmap
		self.width = self.pixmap.height()
		self.height = self.pixmap.height()
		self.resizeImg(self.size)
			# self.update()


	def resizeImg(self,size):
		self.pixmap = self.OGpixmap.scaled(int(self.height*(size/100)),int(self.width*(size/100)),Qt.KeepAspectRatio, Qt.FastTransformation)
		self.size = size
		self.updateImg()

	def getPath(self):
		return self.imgpath

	def setImg(self,imgpath):
		# print('set')
		self.imgpath = imgpath
		self.reloadImg()

	def reloadImg(self):
		self.OGpixmap = QPixmap(self.imgpath)
		self.pixmap = self.OGpixmap
		self.resizeImg(self.size)
		self.updateImg
		()

	def updateImg(self):
		self.setPixmap(self.pixmap)

	def getOriginalWidth(self):
		return self.width
	def getWidth(self):
		return self.pixmap.width()

	def getOriginalHeight(self):
		return self.height
	def getHeight(self):
		return self.pixmap.height()


class App(QWidget):
	def __init__(self):
		super().__init__()
		self.setWindowTitle('pyDither QT')
		self.closeEvent = self.exit
		if len(sys.argv) > 1:
			self.IMPath = sys.argv[1]
		else:
			self.IMPath = 'magick'

		self.imgpath = f'{src_path}no_image_loaded.png'
		self.initUI()

	def exit(self,event):
		for item in os.listdir(src_path):
			if item.startswith('temp.'):
				os.remove(src_path+item)
		# os.remove(src_path+'temp*')
		event.accept()


	def initUI(self):
		# Center image stuff
		self.image = LImage(self.imgpath)
		self.resize(580,320)
		self.ScrollArea = QScrollArea()
		self.ScrollArea.setAlignment(Qt.AlignCenter)
		self.ScrollArea.setWidgetResizable(True)
		self.ScrollArea.setWidget(self.image)

		# Sidebar zoom slider
		self.zoomLabel = QLabel('100%')
		self.zoomSlider = QSlider(Qt.Vertical)
		self.zoomSlider.setSingleStep(1)
		self.zoomSlider.setPageStep(1)
		self.zoomSlider.setMaximum(1000)
		self.zoomSlider.setMinimum(10)
		self.zoomSlider.setValue(100)
		self.zoomSlider.setToolTip('Zoom amount')
		self.zoomSlider.valueChanged.connect(lambda: (self.image.resizeImg(self.zoomSlider.value()), self.zoomLabel.setText(f'{int(self.zoomSlider.value())}%')))
		# self.zoomSlider


		# dither slider, open file dialog, dither method
		self.fileOpenButton = LFileButton(self.loadImage,'🗁')
		self.fileOpenButton.setMaximumWidth(35)
		self.fileOpenButton.setNameFilter('Images (*.png *.jpeg *.jpg *.gif *.apng)')
		self.fileOpenButton.setFileMode(QFileDialog.ExistingFile)
		self.fileOpenButton.setToolTip('Open image')

		self.fileSaveButton = LFileButton(self.saveImage,'🖬')
		self.fileSaveButton.setMaximumWidth(35)
		self.fileSaveButton.setNameFilter('Any file (*)')
		self.fileSaveButton.setFileMode(QFileDialog.AnyFile)
		self.fileSaveButton.setToolTip('Save image with current settings')

		self.ditherSlider = QSlider(Qt.Horizontal)
		self.ditherSlider.setMaximum(64)
		self.ditherSlider.setMinimum(1)
		self.ditherSlider.setValue(32)
		self.ditherSlider.setSingleStep(1)
		self.ditherSlider.setPageStep(1)
		self.ditherSliderLabel = QLabel(str(self.ditherSlider.value()))
		self.ditherComboBox = QComboBox()
		self.ditherComboBox.addItems(['None','FloydSteinberg','Riemersma'])
		self.ditherComboBox.setToolTip('Dithering method')
		self.colorCountLCD = QLCDNumber()
		self.colorCountLCD.display(self.ditherSlider.value())
		self.colorCountLCD.setDigitCount(2)
		self.colorCountLCD.setToolTip('Amount of colors')

		self.ditherComboBox.currentIndexChanged.connect(self.updateImage)
		self.ditherSlider.sliderReleased.connect(self.updateImage)
		self.ditherSlider.valueChanged.connect(lambda: self.colorCountLCD.display(self.ditherSlider.value()))

		# Assembling
		self.lyt_composit = QHBoxLayout()
		self.lyt_bottom = QHBoxLayout()
		self.lyt_center = QVBoxLayout()
		self.lyt_sidebar = QVBoxLayout()
		# self.lyt_top = QHBoxLayout()
		# self.lyt_mid = QVBoxLayout()
		# self.lyt_bot = QHBoxLayout()

		self.lyt_sidebar.addWidget(self.zoomLabel)
		self.lyt_sidebar.addWidget(self.zoomSlider)
		self.lyt_center.addWidget(self.ScrollArea)
		self.lyt_bottom.addWidget(self.fileOpenButton)
		self.lyt_bottom.addWidget(self.colorCountLCD)
		self.lyt_bottom.addWidget(self.ditherSlider)
		self.lyt_bottom.addWidget(self.ditherComboBox)
		self.lyt_bottom.addWidget(self.fileSaveButton)
		# self.setLayout(self.lyt_top)

		self.lyt_center.addLayout(self.lyt_bottom)
		self.lyt_composit.addLayout(self.lyt_center)
		self.lyt_composit.addLayout(self.lyt_sidebar)
		self.setLayout(self.lyt_composit)
		self.show()

	def layoutAdds(self,layout,widgets):
		for i in widgets:
			layout.addWidget(i)

	def getArgs(self,inp,outp):
		return [self.IMPath,'"'+inp+'"','-dither',self.ditherComboBox.currentText(),'-colors',str(self.ditherSlider.value()),'"'+outp+'"']

	def updateImage(self):
		def doit(self):
			extension = self.imgpath.split('.')[-1]
			output = f'{src_path}temp.{extension}'
			args = self.getArgs(self.imgpath,output)
			os.system(' '.join(args))
			self.image.setImg(output)
		# x = Thread(target=doit,args=(self,),daemon=True)
		# x.start()
		doit(self)

	def loadImage(self,imgpath):
		self.imgpath = imgpath[0]
		self.updateImage()

	def saveImage(self,imgpath):
		args = self.getArgs(self.imgpath, imgpath[0])
		os.system(' '.join(args))

src = os.path.abspath(__file__)
if os.name == 'nt':
	src_path = src.replace(src.split('\\')[-1],'')
else:
	src_path = src.replace(src.split('/')[-1],'')

app = QApplication([])
a = App()
app.exec()